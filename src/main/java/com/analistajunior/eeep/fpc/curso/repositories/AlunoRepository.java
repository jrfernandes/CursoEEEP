package com.analistajunior.eeep.fpc.curso.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.analistajunior.eeep.fpc.curso.model.Aluno;

public interface AlunoRepository extends JpaRepository<Aluno, Long>{
	
	public List<Aluno> findByNomeContaining(String nome);
	
	public Aluno findByCpf(String cpf);

}
