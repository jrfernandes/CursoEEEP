package com.analistajunior.eeep.fpc.curso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoEeepApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursoEeepApplication.class, args);
	}
}
