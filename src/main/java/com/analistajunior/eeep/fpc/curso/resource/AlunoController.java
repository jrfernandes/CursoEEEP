package com.analistajunior.eeep.fpc.curso.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analistajunior.eeep.fpc.curso.model.Aluno;
import com.analistajunior.eeep.fpc.curso.services.AlunoService;

@RestController
@RequestMapping("/alunos")
public class AlunoController {
	
	@Autowired
	AlunoService service;
	
	
	@PostMapping
	public ResponseEntity<Aluno> salvar(@RequestBody Aluno aluno){
		Aluno alunoSalvo = service.salvar(aluno);
		return ResponseEntity.ok(alunoSalvo);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Aluno> buscar(@PathVariable Long id){
		Aluno aluno = service.buscarPorId(id);
		return aluno == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(aluno);
	}
	
	@GetMapping(params= "nome")
	public ResponseEntity<List<Aluno>> buscarPorNome(String nome){
		return ResponseEntity.ok(service.buscarPorNome(nome));
	}
	
	@GetMapping(params="cpf")
	public ResponseEntity<Aluno> buscarPorCpf(String cpf){
		return ResponseEntity.ok(service.buscarPorCpf(cpf));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Aluno> atualizar(@PathVariable Long id, @RequestBody Aluno aluno){
		return ResponseEntity.ok(service.atualizar(id, aluno));
	}
	
	@DeleteMapping("/{id}")
	public void deletar(@PathVariable Long id) {
		service.deletarPorId(id);
	}
	
}
