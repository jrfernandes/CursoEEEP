package com.analistajunior.eeep.fpc.curso.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analistajunior.eeep.fpc.curso.model.Aluno;
import com.analistajunior.eeep.fpc.curso.repositories.AlunoRepository;
import com.analistajunior.eeep.fpc.curso.services.exceptions.DataIntegrityException;
import com.analistajunior.eeep.fpc.curso.services.exceptions.ObjectNotFountException;


@Service
public class AlunoService {
	
	@Autowired
	AlunoRepository repo;
	
	public Aluno buscarPorCpf(String cpf) {
		if(repo.findByCpf(cpf) ==  null) {
			throw new ObjectNotFountException("Aluno não encontrado");
		}
		return repo.findByCpf(cpf);
	}
	
	public Aluno salvar(Aluno aluno) {
		if(repo.findByCpf(aluno.getCpf()) != null) {
			throw new DataIntegrityException("CPF já cadastrado");
		}
		return repo.save(aluno);
	}
	
	public Aluno buscarPorId(Long id) {
		Aluno aluno = repo.findOne(id);
		if(aluno==  null) {
			throw new ObjectNotFountException("Aluno não encontrado");
		}
		return aluno;
	}
	
	public List<Aluno> buscarPorNome(String nome){
		List<Aluno> alunos = repo.findByNomeContaining(nome);
		if(alunos.size()==0) {
			throw new ObjectNotFountException("Aluno não encontrado");
		}
		return alunos;
	}
	
	public void deletarPorId(Long id) {
		buscarPorId(id);
		repo.delete(id);
	}
	
	public Aluno atualizar(Long id, Aluno aluno) {
		Aluno alunoSalvo = buscarPorId(id);
		alunoSalvo.setNome(aluno.getNome());
		alunoSalvo.setCpf(aluno.getCpf());
		return repo.save(alunoSalvo);
	}
}
